# minimal-vim
A minimalist setup for vim. This was inspired by [@cassidoo's](https://github.com/cassidoo) [vim-up](https://github.com/cassidoo/vim-up), but aimed at people new to vim. It introduces `:help`, plugins, and some remaps.

I encourage users to explore other vimrcs, but not to add anything unless they have a need for it. Do not blindly copy+paste, and take things slowly. Vim may be very overwhelming at first.

# Setup


1. Download [vim-plug](https://github.com/junegunn/vim-plug)

    The 3 methods below are from junegunn's readme.  
    ###### Unix

    ```sh
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    ```

    ###### Neovim

    ```sh
    curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    ```

    ###### Windows (PowerShell)

    ```powershell
    md ~\vimfiles\autoload
    $uri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    (New-Object Net.WebClient).DownloadFile($uri, $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath("~\vimfiles\autoload\plug.vim"))
    ```
2. If you have a current `.vimrc`, and would like to try this out, save a copy of it. Grab the text in the `vimrc` file in this repo, and place it in `~/.vimrc`.

3. Open `.vimrc` and run `:PlugInstall`.


## TODO
- [ ] Add an actual installation process that allows for an easy add/remove of the `vimrc` file
and allows for a `git pull` update.
- [ ] finsih accompanying blog post describing reasoning and intended audience. 
- [ ] have fun
- [ ] consider no plugins
