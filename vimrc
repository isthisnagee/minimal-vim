"
" @isthisnagee, last update: Sep 15, 2017
"

" Type `:help intro` and `:help help` to learn how to navigate help pages.
" Basics: You can jump to a reference (an underline) with `CTRL [` and jump
" back with `CTRL T`.
" `:h` is equivalent to `:help`

set nocompatible " :h compatible
" make vim less vi-compatible

syntax enable " :h syn-qstart
" enable syntax

set expandtab " :h expandtab
" Pressing <Tab> inserts spaces

set smarttab " :h smarttab
" Pressing <Tab> inserts blanks according to surrounding usage

set tabstop=4 " :h tabstop
" 1 tab == 4 spaces

set shiftwidth=4 " :h shiftwidth
" 1 tab == 4 spaces

set number " :h number
" Insert line numbers

set showcmd " :h showcmd
" show partial command in last line of screen.

set wildmenu " :h wildmenu
" enhanced command  line completion

set showmatch " :h showmatch
" 'peek' at matching bracket when inserting closing bracket

set ruler " :h ruler
" show line and column number

set incsearch " :h incsearch
" show matches as you type

set visualbell " :h visualbell
" don't beep.

set backspace=indent,eol,start " :h backspace
" make backspace behave properly.
" The above does the following
"     indent   => allow backspacing over autoindent
"     eol      => allow backspacing over line breaks (join lines)
"     start    => allow backspacing over the start of insert
" See `:help bs` (shorthand for :help backspace) for more.

filetype indent on
filetype plugin indent on

" ---------Plugins----------------------
call plug#begin('~/.vim/plugged')
" all of these can be viewed by going to http://github.com/<text-in-quotes>
" for example, http://github.com/tpope/vim-vinegar, where tpope/vim-vinegar
" is the text in quotes.
" Besides each plugin is a small explanation for what they are, below them
" is the command or url to access documentation.
" Vim-Plug Intro:
"     Reloading the file
"         1. save the file
"             - go to normal mode (press the escape key)
"             - press the colon character (:)
"             - type the letter w.
"             - press enter
"         2. Load the file
"             - make sure you're still in normal mode (press the escape key)
"             - press the colon character (:)
"             - type the following (without the quotes): "source %"
"                 - The source command (:h load-vim-script) will load the file.
"                 - The percent (%) is shorthand for the current file (:h current-file)
"
"     Removing Plugins
"         0. Make sure you're in normal mode (press the escape key)
"         1. Delete the line with the plugin you want to remove (use `dd` to remove,
"         `u` if you change your mind).
"         2. Follow the steps in "Reloading the file" 
"         3. Then type colon (:) followed by "PlugClean", then press enter
"     Adding Plugins
"         0. Make sure you're in normal mode (press the escape key)
"         1. If you find a plugin you like on github, then add the line
"           Plug '<github-username>/<repo-name>'
"         2. Follow the steps in "Reloading the file"
"         3. Press the colon (:) character, then type "PlugInstall", then press enter.
"     Updating Plugins
"         0. Make sure you're in normal mode (press the escape key)
"         1. Press the colon (:) character
"         3. Type "PlugUpdate" and press enter.
"
"   Consult  http://github.com/junegunn/vim-plug for more.

Plug 'tpope/vim-vinegar'
" https://github.com/tpope/vim-vinegar/blob/master/README.markdown
" `-` in normal mode will open up a directory browser

Plug 'tpope/vim-commentary'
" :help commentary
" gc<motion> to comment multiple things, gcc for one line

Plug 'tpope/vim-surround'
" :help surround
" allows for changing 'surrounds', like `[`, `(`, `'`, ...

Plug 'tpope/vim-repeat'
" :help repeat
" allows for smarter repeats with `.`

call plug#end()
" ---------END Plugins------------------


" persistent undo
set undofile
set undodir=$HOME/.vim/undo
" ^ make sure this directory exists. `mkdir .vim/undo` should create it.


" --------Brace Expansion---------------
"  The following are mappings for insert mode, so typing what comes
"  before the arrow (`=>`) does what happends after the `=>`

" {<enter> => adds starting/closing brakcets
inoremap {<CR> {<CR>}<Esc>O

" {;       => adds opening/closing bracket with `;` after
inoremap {; {<CR>};<Esc>O

" {,       => adds opening/closing brackets with `,` after
inoremap {, {<CR>},<Esc>O

" ({       => adds `({` then closes with `})`
inoremap ({ ({<CR>})<Esc>O

" {)       => adds a `{` then a closing `});`
inoremap {) {<CR>});<Esc>O
" --------END Brace Expansion-----------

" --------Panes-------------------------

" Use `:split` or `:vpslit` to open horizontal and vertical panes,
" respectively. You can use CTRL W<direction> to move between panes,
" and <direction> is one of h,j,k,l. Alternatively, CTRL W W will cycle
" between panes.
"
" Go to `:h split` to learn more.

" Below are some shortcuts, and their explanation is below.
nnoremap <expr><silent> <Bar> v:count == 0 ? '<C-W>v<C-W><Right>' : ':<C-U>normal! 0'.v:count.'<Bar><CR>'
" `|` in normal mode opens a new vertical pane.
" press `-` after to open direcotry browser
nnoremap <expr><silent> _ v:count == 0 ? '<C-W>s<C-W><Down>'  : ':<C-U>normal! '.v:count.'_<CR>'
" `_` in normal mode opens a new horizontal pane.
" press `-` after to open direcotry browser

" --------END Panes---------------------
